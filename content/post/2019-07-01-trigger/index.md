+++
title = "Three Strikes"
date = "2019-07-01"
description = "You always get a free trip to solitary if you're a furry."
tags = ["ace combat", "furry"]
categories = ["art"]
image = "cover.png"
slug = "trigger"
+++

Stick with him and you'll probably crash.

![](line.png) ![](trigger.png)
