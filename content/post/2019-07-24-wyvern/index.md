+++
title = "X-02F Flying Wyvern"
date = "2019-07-24"
description = "Next generation fighter aircraft"
tags = ["monster hunter", "ace combat"]
categories = ["art"]
image = "cover.png"
slug = "wyvern"
+++

Capable of autonomous or manned flight, this next generation of fighter aircraft will be the main antagonist of Ace Combat 8.
The X-02F Flying Wyvern is a product of Belkan bioengineering in an effort to exact revenge against Osea a few decades after the Belkan's bitter defeat.

![Sketch. I don't know why I did this](x-02f.png) ![I completely regret doing this.](x-02f_colored.png)
