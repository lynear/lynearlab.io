+++
title = "Valstrax"
date = "2019-08-16"
description = "Mobius 1, engage!"
tags = ["monster hunter", "ace combat"]
categories = ["art"]
image = "cover.png"
slug = "valstrax"
+++

![Sketch. Note to self, always zoom out to have awareness of line thickness in the big picture.](strax.png) ![Colored version. I'm really happy how this one turned out!](strax_colored.png)

After defeating another Free Erusea uprising in 2014, Mobius 1 finally retired from his military duties.
Unfortunately, he was then captured by Belkan scientists.
He was subjected unto different experiments and his consciousness was transferred into a bioengineered dragon
capable of flying at mach speed.

