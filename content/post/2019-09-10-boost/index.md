+++
title = "Boost me up, fam!"
date = "2019-09-10"
description = "How come larian never thought of this"
tags = ["divinity original sin 2"]
categories = ["art"]
image = "cover.png"
slug = "boosted"
+++

just a dumb thought while playing D:OS 2

![Sketch](sketch.png) ![despite thinking of this brilliant idea, she regrets having to carry and balance an entire person on top of her.](boost.png)
