+++
title = "234 ±5%"
date = "2019-09-16"
description = "is it even possible to be a voltage source and a resistor at the same time?"
tags = ["monster hunter"]
categories = ["art"]
image = "cover.png"
slug = "resistorlagiacrus"
+++

Is this even possible? I'm not an electronics engineer so I'm pretty clueless

![will this wiring diagram result into a short circuit?](lagiacrus.png)
