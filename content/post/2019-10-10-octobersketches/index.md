+++
title = "October sketches"
date = "2019-10-10"
description = "some random sketches I did during this month"
tags = ["furry", "monster hunter"]
categories = ["art"]
image = "cover.png"
slug = "octobersketches"
+++

i wish i had more time to practice drawing human anatomy, but since my schedule and every other
brick wall hindering me aren't giving me time, I unfortunately have to go with these.

![lord, sana biyayaan niyo po ako ng isang magandang asawa. Gets mo ba?](sawa.png)
![inamogago](laugh.png)
![pero parang may tama yung gumawa nito eh](tama.png)

![when u flip and land a hit on hunter who's already low on hp](smug_rathian.png)
![peace out yo](peace_out.png)
![parang ganito yung naririnig ko parati sa bibig ng iba eh](bakit.png)
