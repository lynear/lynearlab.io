+++
title = "Ginhawa"
date = "2019-10-13"
description = "May ginhawa na sa wakas!"
tags = ["furry", "personal"]
categories = ["art"]
image = "cover.png"
slug = "ginhawa"
+++

Finally, after an entire month of non-stop long exams and projects, I could finally take a break from acads!

Layla can go home and take a long rest at last. Commuting back home, Layla sure saw a lot of restaurants along Maginhawa street that she even thought of dining out along with her friends!

… If only she had the money to do so…

I guess that’s life for Layla! Even the tricycle driver knew that she doesn’t have the money for the fare and bailed out on her!

![tabi po ng savemore kuya](roughsketch.png)
![the colors were fine, but the blocking felt off](roughcolor.png)

![so that's why! the character scaling is too small for the entire canvas](shaded.png)
![BAKIT NAWAWALA YUNG TSUPER?](final.png)
