+++
title = "Can the world please just stop for a moment?"
date = "2019-12-06"
description = "Hi. I'm still alive after all these months. I've been working on sorting out my life because I realized my time is really goddamn precious"
tags = ["monster hunter"]
categories = ["art"]
image = "cover.png"
slug = "novembersketches"
+++

Weeks of nonstop exams and electronics took a toll on my motivation. Plus, I realized that I always regretted
wasting my time on worthless youtube videos and other distractions on the internet.

![bruhhhhhthian](dabian.png)
![because odogaron red dog and galm emblem red dog yes](ace_styles.png)
