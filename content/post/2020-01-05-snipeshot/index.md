+++
title = "Inteleon used Snipe Shot!"
date = "2020-01-05"
description = "just a redo of an old piece, gamefreak should add in inteleon with a ptrs-41 tbh"
tags = ["pokemon"]
categories = ["art"]
image = "cover.png"
slug = "snipeshot"
+++

just imagine trying to jump while encumbered with a 20kg-heavy anti-tank rifle

![](pls_judge.png)
![](snipe_shot.png)
