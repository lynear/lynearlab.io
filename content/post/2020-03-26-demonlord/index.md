+++
title = "Demon Lord"
date = "2020-03-26"
description = ""
tags = ["monster hunter", "ace combat"]
categories = ["art"]
image = "cover.png"
slug = "demonlord"
+++

This is where it all started.

![](roughsketch.png)
![](sketch.png)
![](line.png)

![](transparent.png)
![](final.png)
