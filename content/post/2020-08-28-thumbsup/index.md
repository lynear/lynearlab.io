+++
title = "Thumbs up"
date = "2020-08-28"
description = ""
tags = ["furry", "oc"]
categories = ["art"]
image = "cover.png"
slug = "thumbsup"
+++

![](roughsketch.png)
![](sketch.png)
![](line.png)

![](beta1.png)
![](beta2.png)
![](final.png)
