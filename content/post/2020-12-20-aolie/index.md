+++
title = "Ao Lie"
date = "2020-12-20"
description = "seafood man"
tags = ["scaly"]
categories = ["art"]
image = "cover.png"
slug = "aolie"
+++

I've been enjoying Fei Ren Zai recently.
I really like the show's short format, perfect for a slice of life animation.

Here's Ao Lie!

![](sketch.png)
![](final.png)
