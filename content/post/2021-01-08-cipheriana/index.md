+++
title = "Cipheriana"
date = "2021-01-08"
description = "half-body sprite for Legi-Cipher"
tags = ["monster hunter", "ace combat"]
categories = ["art"]
image = "cover.png"
slug = "cipheriana"
+++

Something I drew thanks to Spinel's idea of an Ace Combat x Monster Hunter dating sim
where you romance Legiana-Cipher.

(there's a route where she kills you, she still hasn't lost her mercenary instincts)

!["You didn't kill enough Belkans, anon"](disappointed.png)
![](sketch.png)

![](line.png)
![There's really something off with the colored version, I'll redo this in the future...](final.png)
