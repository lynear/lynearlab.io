+++
title = "Layla sketches"
date = "2021-01-20"
description = "Trying to pose the body"
tags = ["furry", "oc"]
categories = ["art"]
image = "cover.png"
slug = "laylasketches"
+++

Trying to practice proportions and basics. Honestly don't know what I was going for here.

I still don't know how to pose the limbs and arms.

![](longlynxrough.png)
![](longlynx.png)
![](apose.png)
![](handsup.png)
