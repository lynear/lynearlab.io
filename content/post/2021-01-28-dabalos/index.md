+++
title = "Dabbing Rathalos"
date = "2021-01-28"
description = "The sequel to dabbing Rathian"
tags = ["monster hunter", "joke"]
categories = ["art"]
image = "cover.png"
slug = "dabalos"
+++

Wouldn't be complete without Rathi-chan's husbando, wouldn't it?

![The torso is scrunched](roughsketch.png)
![Fixed it somewhat](sketch.png)
![](line.png)

![](final.png)
![No emojis](noemoji.png)

![Mating dance](couple.png)
