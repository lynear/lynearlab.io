+++
title = "Valentines gift"
date = "2021-02-13"
description = "Gift for Spinel and Sinderion"
tags = ["scaly"]
categories = ["art"]
image = "cover.png"
slug = "valentines"
+++

Yo Spinel, thanks again for being a great friend!

![](sketch.png)
![](line.png)

![](final.png)
