+++
title = "A very jolly lynx"
date = "2021-02-23"
description = "man I wish I could be as happy as her 😞"
tags = ["furry", "personal"]
categories = ["art"]
image = "cover.png"
slug = "happylynx"
+++

Oh man, even if I'm still trying to study drawing females,
I'm really proud how this one turned out!


Although I wish I was more consistent with how I draw her,
I feel like it's just luck this time around.

![Even now, I'm still struggling with the 3/4 view](extra.png)
![](sketch.png)
![](guide.png)
![](line.png)

![](final.png)
![](finalfixed.png)
