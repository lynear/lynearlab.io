+++
title = "You're pretty good!"
date = "2021-03-18"
description = ""
tags = ["furry", "oc"]
categories = ["art"]
image = "cover.png"
slug = "prettygood"
+++

![](roughsketch.png)
![](sketch.png)
![](line.png)

![](final.png)
![](finalfixed.png)
