+++
title = "Sleeping"
date = "2021-03-20"
description = "Don't you drift away..."
tags = ["furry", "oc"]
categories = ["art"]
image = "cover.png"
slug = "sleeping"
+++

![](roughsketch.png)
![](sketch.png)
![](line.png)

![](final.png)
