+++
title = "Special Eyes"
date = "2021-03-30"
description = "Trying new/different style for female eyes"
tags = ["furry", "oc", "study"]
categories = ["art"]
image = "cover.png"
slug = "specialeyes"
+++

![](cartoon1.png)
![](cartoon2.png)
![](raen.png)
![](kemono.png)

![](eternityzinogre.png)
![](kerotzuki.png)
![](kerotzukibig.png)
![](nicnak.png)

![](final.png)
