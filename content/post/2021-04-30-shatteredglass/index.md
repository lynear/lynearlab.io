+++
title = "Shattered Glass"
date = "2021-04-30"
description = "And only heaven knows..."
tags = ["monster hunter", "ace combat"]
categories = ["art"]
image = "cover.png"
slug = "shatteredglass"
+++

![](roughsketch.png)
![](sketch1.png)
![](sketch2.png)
![](line.png)

![](final.png)
![](finalnoise.png)

--- 

{{< youtube TAGrGP5KBc8 >}}
