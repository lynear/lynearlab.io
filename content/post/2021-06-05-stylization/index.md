+++
title = "Stylization"
date = "2021-06-05"
description = "Trying to give Layla-chan a bit more oomph"
tags = ["furry", "oc", "study"]
categories = ["art"]
image = "cover.png"
slug = "stylization"
+++

![](mouth.png)
![](quickbody.png)
![](sketch.png)

![](final.png)
