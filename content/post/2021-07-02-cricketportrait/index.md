+++
title = "WoF artworks"
date = "2021-07-02"
description = "This series is a really fun read!"
tags = ["wof", "wingsoffire"]
categories = ["art"]
image = "cover.png"
slug = "cricketportrait"
+++

![](scrap.png)
![ugh, front facing dragons are really hard](frontface.png)

--- 
![possibly the world's worst crossover idea yet](sketch1.png)
![FRUIT EEAAAAAAAATER](sketch2.png)

---

![](roughsketch.png)
![](sketch.png)
![](line.png)

---
![cricket my beloved](final.png)
![with noise](finalnoise.png)
